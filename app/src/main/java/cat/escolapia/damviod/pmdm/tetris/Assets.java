package cat.escolapia.damviod.pmdm.tetris;

import cat.escolapia.damviod.pmdm.framework.Pixmap;
import cat.escolapia.damviod.pmdm.framework.Sound;

public class Assets {
    public static Pixmap background;
    public static Pixmap logo;
    public static Pixmap mainMenu;
    public static Pixmap buttons;
    public static Pixmap numbers;
    public static Pixmap ready;
    public static Pixmap pause;
    public static Pixmap gameOver;
    public static Pixmap piece1;
    public static Pixmap piece2;
    public static Pixmap piece3;
    public static Pixmap piece4;
    public static Pixmap piece5;
    public static Pixmap piece6;
    public static Pixmap piece7;
    public static Pixmap block;

    public static Sound clickSound;
    public static Sound eat;
    public static Sound gameOverSound;
}
