package cat.escolapia.damviod.pmdm.tetris;

import cat.escolapia.damviod.pmdm.framework.Game;
import cat.escolapia.damviod.pmdm.framework.Graphics;
import cat.escolapia.damviod.pmdm.framework.Screen;
import cat.escolapia.damviod.pmdm.framework.Graphics.PixmapFormat;

public class LoadingScreen extends Screen {
    public LoadingScreen(Game game) {
        super(game);
    }

    public void update(float deltaTime) {
        Graphics g = game.getGraphics();
        Assets.background = g.newPixmap("background.png", PixmapFormat.RGB565);
        Assets.logo = g.newPixmap("logo.png", PixmapFormat.ARGB4444);
        Assets.mainMenu = g.newPixmap("mainmenu.png", PixmapFormat.ARGB4444);
        Assets.buttons = g.newPixmap("buttons.png", PixmapFormat.ARGB4444);
        Assets.numbers = g.newPixmap("numbers.png", PixmapFormat.ARGB4444);
        Assets.ready = g.newPixmap("ready.png", PixmapFormat.ARGB4444);
        Assets.pause = g.newPixmap("pausemenu.png", PixmapFormat.ARGB4444);
        Assets.gameOver = g.newPixmap("gameover.png", PixmapFormat.ARGB4444);
        Assets.piece1 = g.newPixmap("piece1.png", PixmapFormat.ARGB4444);
        Assets.piece2 = g.newPixmap("piece2.png", PixmapFormat.ARGB4444);
        Assets.piece3 = g.newPixmap("piece3.png", PixmapFormat.ARGB4444);
        Assets.piece4 = g.newPixmap("piece4.png", PixmapFormat.ARGB4444);
        Assets.piece5 = g.newPixmap("piece5.png", PixmapFormat.ARGB4444);
        Assets.piece6 = g.newPixmap("piece6.png", PixmapFormat.ARGB4444);
        Assets.piece7 = g.newPixmap("piece7.png", PixmapFormat.ARGB4444);
        Assets.block = g.newPixmap("block.png", PixmapFormat.ARGB4444);
        Assets.clickSound = game.getAudio().newSound("click.ogg");
        Assets.gameOverSound = game.getAudio().newSound("bitten.ogg");
        Settings.load(game.getFileIO());

        game.setScreen(new MainMenuScreen(game));

    }
    
    public void render(float deltaTime) {

    }

    public void pause() {

    }

    public void resume() {

    }

    public void dispose() {

    }
}
