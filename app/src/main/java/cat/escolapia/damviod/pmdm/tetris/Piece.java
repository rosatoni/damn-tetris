package cat.escolapia.damviod.pmdm.tetris;

public class Piece implements Cloneable {

    public int type;
    public int currX  = 0;
    public int currY  = 0;
    private int coords[][];

    private int[][][] coordsTable = new int[][][] {
            {},
            {
                    { 0, 1, 0 ,0 },
                    { 0, 1, 0 ,0 },
                    { 0, 1, 0 ,0 },
                    { 0, 1, 0, 0 }
            },//Linea
            {
                    { 0, 0, 0 ,0 },
                    { 0, 1, 1 ,0 },
                    { 0, 1, 1 ,0 },
                    { 0, 0, 0, 0 }
            },//Quadrat
            {
                    { 0, 1, 0 ,0 },
                    { 0, 1, 1 ,0 },
                    { 0, 0, 1 ,0 },
                    { 0, 0, 0, 0 }
            },//S
            {
                    { 0, 0, 1 ,0 },
                    { 0, 1, 1 ,0 },
                    { 0, 1, 0 ,0 },
                    { 0, 0, 0, 0 }
            },//2
            {
                    { 0, 1, 1 ,0 },
                    { 0, 1, 0 ,0 },
                    { 0, 1, 0 ,0 },
                    { 0, 0, 0, 0 }
            },//L revés
            {
                    { 0, 1, 1 ,0 },
                    { 0, 0, 1 ,0 },
                    { 0, 0, 1 ,0 },
                    { 0, 0, 0, 0 }
            },//L
            {
                    { 0, 1, 0 ,0 },
                    { 1, 1, 1 ,0 },
                    { 0, 0, 0 ,0 },
                    { 0, 0, 0, 0 }
            } // 3
    };

    public Piece(int newType, int initX, int initY) {
        type =  newType;
        currX = initX;
        currY = initY;
        coords = new int[4][4];

        for (int i = 0; i < 4 ; i++) {
            for (int j = 0; j < 4; ++j) {
                coords[i][j] = coordsTable[newType][i][j];
            }
        }
    }

    public void moveLeft() {
        currX--;
    }
    
    public void moveRight() {
        currX++;
    }

    public void advance() {
        currY++;
    }

    public void rotate() {
        int rotatedCoords[][] = new int[4][4];

        for (int i = 0; i < 4; ++i) {
            for (int j = 0; j < 4; ++j) {
                rotatedCoords[j][i] = coords[3 - i][j];
            }
        }
        coords = rotatedCoords;
    }

    public int getPos(int x, int y) {
        return coords[x][y];
    }


    public int getMaxY() {
        int max_y = 0;
        Boolean found= false;
        for (int y = 3; y >= 0 && !found; --y) {
            for (int x = 0; x < 4; ++x) {
                if (coords[x][y]!=0) {
                    max_y = y ;
                    found = true;
                }
            }
        }
        return max_y + currY;
    }

    public int getMinX() {
        int min_x  = 0;
        Boolean found = false;
        for (int x = 0; x < 4 && !found; ++x) {
            for (int y = 0; y < 4; ++y) {
                if (coords[x][y]!=0) {
                    min_x = x ;
                    found = true;
                }
            }
        }
        return min_x + currX;
    }

    public int getMaxX() {
        int max_x = 0;
        Boolean found= false;
        for (int x = 3; x >= 0 && !found; --x) {
            for (int y = 0; y < 4; ++y) {
                if (coords[x][y]!=0) {
                    max_x = x ;
                    found = true;
                }
            }
        }
        return max_x + currX;
    }

    public Object clone() throws CloneNotSupportedException{
        return super.clone();
    }
}

