package cat.escolapia.damviod.pmdm.tetris;

import android.util.Log;

import java.util.ArrayList;
import java.util.Random;

public class Board {
    static final int BOARD_WIDTH = 10;
    static final int BOARD_HEIGHT = 14;
    static final int SCORE_INCREMENT = 10;
    static final float TICK_INITIAL = 0.6f;
    static final float TICK_DECREMENT = 0.02f;

    public Piece currPiece;
    public int fields[][] = new int[BOARD_WIDTH][BOARD_HEIGHT];

    public boolean gameOver = false;
    public int score = 0;
    public int totalLines = 0;

    Random random = new Random();
    float tickTime = 0;
    float tick = TICK_INITIAL;

    public Board() {
        initBoard();
        score = 0;
        totalLines = 0;
        if (!newRandomPiece()) {
            gameOver = true;
        } else {
            gameOver = false;
        }
    }

    private boolean newRandomPiece() {
        int type = Math.abs(random.nextInt()) % 7 + 1; // entre 1 i 7
        currPiece = new Piece(type, (BOARD_WIDTH/2) - 2, -1);
        return fitsInBoard(currPiece);
    }

    public void update(float deltaTime) {
        if (gameOver) return;

        tickTime += deltaTime;
        while (tickTime > tick) {
            tickTime -= tick;
            boolean canAdvance = advancePiece();
            if (canAdvance) {
                score = score + 1;
            } else {
                addPieceToBoard(currPiece);
                ArrayList fullLines = checkFullLines();
                if (fullLines.size() > 0) {
                    removeLines(fullLines);
                    score = score + (fullLines.size() * SCORE_INCREMENT);
                    totalLines = totalLines + fullLines.size();
                    tick -= TICK_DECREMENT;
                }
                if (!newRandomPiece()) {
                    gameOver = true;
                    return;
                }
            }
        }
    }

    public void initBoard() {
        for (int i = 0; i < BOARD_WIDTH; i++) {
            for (int j = 0; j < BOARD_HEIGHT; j++) {
                fields[i][j] = 0;
            }
        }
    }

    private boolean advancePiece() {
        if (currPiece.getMaxY() >= (BOARD_HEIGHT -1) ){
            return false;
        }
        Piece newPiece = null;
        try {
            newPiece = (Piece) currPiece.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        newPiece.advance();
        Boolean fits = fitsInBoard(newPiece);
        if (fits==true) {
            currPiece.advance();
        }
        return fits;
    }

    public boolean movePieceToTheLeft() {
        if (currPiece.getMinX()<=0) return false;
        Piece newPiece = null;
        try {
            newPiece = (Piece) currPiece.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        newPiece.moveLeft();
        if (fitsInBoard(newPiece)) {
            currPiece = newPiece;
            return true;
        } else {
            return false;
        }
    }

    public boolean movePieceToTheRight() {
        if (currPiece.getMaxX()>=BOARD_WIDTH -1) {
            return false;
        }
        Piece newPiece = null;
        try {
            newPiece = (Piece) currPiece.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        newPiece.moveRight();
        if (fitsInBoard(newPiece)) {
            currPiece = newPiece;
            return true;
        } else {
            return false;
        }
    }

    public boolean dropPiece() {
        if (currPiece.getMaxY() >= (BOARD_HEIGHT -1) ){
            return false;
        }
        Piece newPiece = null;
        Piece newPieceOld = null;
        try {
            newPiece = (Piece) currPiece.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        newPiece.advance();
        Boolean fits = fitsInBoard(newPiece);
        while (fits && newPiece.getMaxY() < (BOARD_HEIGHT -1)){
            newPieceOld = newPiece;
            newPiece.advance();
            fits = fitsInBoard(newPiece);
        }
        if (newPieceOld != null) {
            currPiece = newPieceOld ;
            return true;
        } else {
            return false;
        }

    }

    public boolean rotatePiece() {

        Piece newPiece = null;
        try {
            newPiece = (Piece) currPiece.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        newPiece.rotate();
        if (fitsInBoard(newPiece) && newPiece.getMinX() >=0 && newPiece.getMaxX() < BOARD_WIDTH) {
            currPiece = newPiece;
            return true;
        } else {
            return false;
        }
    }

    private Boolean fitsInBoard(Piece piece) {
        Boolean fits = true;
        for (int i = 0; i < 4 && fits; i++) {
            for (int j = 0; j < 4; j++) {
                if (piece.getPos(i, j)!=0) {
                    if (piece.currY + j >= 0 && piece.currX + i >= 0 && piece.currX + i < (BOARD_WIDTH ) && piece.currY + j < (BOARD_HEIGHT )) {
                        if (fields[piece.currX + i][piece.currY + j] != 0){
                            fits = false;
                            break;
                        }
                    }
                }
            }
        }
        return fits;
    }

    private ArrayList<Integer> checkFullLines() {
        int tmp = 0;
        ArrayList<Integer> lines = new ArrayList<Integer>();
        for (int y = 0; y < BOARD_HEIGHT; y++) {
            tmp = 0;
            for (int x = 0; x < BOARD_WIDTH ;x++) {
                if (fields[x][y]!=0) tmp++;
            }
            if (tmp==BOARD_WIDTH) lines.add(y);
        }
        return lines;
    }

    private void removeLines(ArrayList<Integer> lines) {
        for ( int y : lines) {
            removeLine(y);
        }
    }

    private void removeLine(int line) {
        for (int y = line; y > 0; y--) {
            for (int x = 0; x < BOARD_WIDTH ;x++) {
                fields[x][y] = fields[x][y - 1];
            }
        }
    }

    private void addPieceToBoard(Piece piece) {
        int a = 0;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                if (piece.getPos(i, j)!=0) {
                    if ((piece.currX + i >= 0) &&  (piece.currX + i < BOARD_WIDTH) && (piece.currY + j >= 0)&& (piece.currY + j < BOARD_HEIGHT) ) { //Només podem posar valors dins de l'array
                            fields[piece.currX + i][piece.currY + j] = piece.type;
                    };
                }
            }
        }
    }
}
