package cat.escolapia.damviod.pmdm.tetris;

import java.util.List;

import android.graphics.Color;
import android.util.Log;

import cat.escolapia.damviod.pmdm.framework.Game;
import cat.escolapia.damviod.pmdm.framework.Graphics;
import cat.escolapia.damviod.pmdm.framework.Input.TouchEvent;
import cat.escolapia.damviod.pmdm.framework.Pixmap;
import cat.escolapia.damviod.pmdm.framework.Screen;

public class GameScreen extends Screen {
    enum GameState {
        Ready,
        Running,
        Paused,
        GameOver
    }

    static final int FIELD_WIDTH = 30;
    float countDown = 0;
    Pixmap[] piecePixmaps;

    GameState state = GameState.Ready;
    Board board;

    public GameScreen(Game game) {
        super(game);
        board = new Board();
        //init
        countDown = 4;
        piecePixmaps = new Pixmap[8];
        piecePixmaps[0] = Assets.piece1;
        piecePixmaps[1] = Assets.piece2;
        piecePixmaps[2] = Assets.piece3;
        piecePixmaps[3] = Assets.piece4;
        piecePixmaps[4] = Assets.piece5;
        piecePixmaps[5] = Assets.piece6;
        piecePixmaps[6] = Assets.piece7;
        Pixmap blockPixmap = Assets.block;
    }

    @Override
    public void update(float deltaTime) {
        List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
        game.getInput().getKeyEvents();
        
        if(state == GameState.Ready)
            updateReady(touchEvents, deltaTime);
        if(state == GameState.Running)
            updateRunning(touchEvents, deltaTime);
        if(state == GameState.Paused)
            updatePaused(touchEvents);
        if(state == GameState.GameOver)
            updateGameOver(touchEvents);        
    }

    private void updateReady(List<TouchEvent> touchEvents, float deltaTime) {
        countDown -= deltaTime;
        if(touchEvents.size() > 0 || countDown <= 0.0) state = GameState.Running;
    }

    private void updateRunning(List<TouchEvent> touchEvents, float deltaTime) {
        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if(event.type == TouchEvent.TOUCH_UP) {
                if(event.x < 64 && event.y < 64) {
                    Assets.clickSound.play(1);
                    state = GameState.Paused;
                    return;
                }
            }
            if(event.type == TouchEvent.TOUCH_DOWN) {
                if(event.x < 64 && event.y > 416) {
                    board.movePieceToTheLeft();
                }
                if(event.x > 256 && event.y > 416) {
                    board.movePieceToTheRight();
                }
                if(event.x > 128 &&  event.x < 192  && event.y > 416) {
                    board.rotatePiece();
                }
                if(inBounds(event, board.currPiece.currX * FIELD_WIDTH , board.currPiece.currY * FIELD_WIDTH, FIELD_WIDTH * 4, FIELD_WIDTH * 4) ) {
                    board.dropPiece();
                }
            }
        }
        
        board.update(deltaTime);

        if(board.gameOver) {
            Assets.gameOverSound.play(1);
            state = GameState.GameOver;
        }

    }

    private void updatePaused(List<TouchEvent> touchEvents) {
        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if(event.type == TouchEvent.TOUCH_UP) {
                if(event.x > 80 && event.x <= 240) {
                    if(event.y > 100 && event.y <= 148) {
                        Assets.clickSound.play(1);
                        state = GameState.Running;
                        return;
                    }                    
                    if(event.y > 148 && event.y < 196) {
                        Assets.clickSound.play(1);
                        game.setScreen(new MainMenuScreen(game)); 
                        return;
                    }
                }
            }
        }
    }

    private boolean inBounds(TouchEvent event, int x, int y, int width, int height) {
        Log.v("TETRIS", "x=" + event.x + " y=" + event.y);
        Log.v("TETRIS", "minx=" + x + " maxx=" + (x+width));
        Log.v("TETRIS", "miny=" + y + " maxy=" + (y + height));
        if(event.x > x && event.x < x + width - 1 &&
                event.y > y && event.y < y + height - 1)
            return true;
        else
            return false;
    }

    private void updateGameOver(List<TouchEvent> touchEvents) {
        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if(event.type == TouchEvent.TOUCH_UP) {
                    Assets.clickSound.play(1);
                    game.setScreen(new MainMenuScreen(game));
                    return;
            }
        }
    }

    @Override
    public void render(float deltaTime) {
        Graphics g = game.getGraphics();
        
        g.drawPixmap(Assets.background, 0, 0);
        g.drawLine(0, 412, 450, 412, Color.DKGRAY);

        drawBoard(board);
        if(state == GameState.Ready) 
            drawReadyUI();
        if(state == GameState.Running)
            drawRunningUI();
        if(state == GameState.Paused)
            drawPausedUI();
        if(state == GameState.GameOver)
            drawGameOverUI();

    }

    private void drawBoard(Board board) {
        Graphics g = game.getGraphics();
        Piece piece = board.currPiece;

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                if (piece.getPos(i, j)!=0) {
                    int x = (piece.currX + i ) * FIELD_WIDTH;
                    int y = (piece.currY + j) * FIELD_WIDTH;
                    g.drawPixmap(piecePixmaps[piece.type - 1], x, y, 0, 0 , FIELD_WIDTH, FIELD_WIDTH);
                }
            }
        }

        for (int i = 0; i < board.BOARD_WIDTH; i++) {
            for (int j = 0; j < board.BOARD_HEIGHT ;j++) {
                if (board.fields[i][j]!=0) {
                    int x = i * FIELD_WIDTH;
                    int y = j * FIELD_WIDTH;
                    g.drawPixmap(piecePixmaps[board.fields[i][j] -1], x, y, 30, 0, FIELD_WIDTH, FIELD_WIDTH);
                }

            }
        }
    }

    private void drawReadyUI() {
        Graphics g = game.getGraphics();
        g.drawPixmap(Assets.ready, 47, 100);
        drawText(g, Integer.toString((int)countDown), g.getWidth() / 2 - 30 , 180);
    }
    
    private void drawRunningUI() {
        Graphics g = game.getGraphics();
        g.drawPixmap(Assets.buttons, 0, 0, 64, 128, 64, 64); //Pause
        g.drawPixmap(Assets.buttons, 0, 416, 64, 64, 64, 64); //Left
        g.drawPixmap(Assets.buttons, 256, 416, 0, 64, 64, 64); //Right
        g.drawPixmap(Assets.buttons, 128, 416, 64, 0, 64, 64); // Rotate

        String totalLines = Integer.toString(board.totalLines);
        drawText(g, totalLines, g.getWidth() - 30 - totalLines.length() * 20 / 2, 25);
    }
    
    private void drawPausedUI() {
        Graphics g = game.getGraphics();
        g.drawPixmap(Assets.pause, 80, 100);

    }

    private void drawGameOverUI() {
        Graphics g = game.getGraphics();
        g.drawPixmap(Assets.gameOver, 80, 200);

        String totalLines = Integer.toString(board.totalLines);
        drawText(g, totalLines, g.getWidth() - 30 - totalLines.length() * 20 / 2, 25);
    }
    
    public void drawText(Graphics g, String line, int x, int y) {
        int len = line.length();
        for (int i = 0; i < len; i++) {
            char character = line.charAt(i);

            if (character == ' ') {
                x += 20;
                continue;
            }

            int srcX = 0;
            int srcWidth = 0;
            if (character == '.') {
                srcX = 200;
                srcWidth = 10;
            } else {
                srcX = (character - '0') * 20;
                srcWidth = 20;
            }

            g.drawPixmap(Assets.numbers, x, y, srcX, 0, srcWidth, 32);
            x += srcWidth;
        }
    }

    @Override
    public void pause() {
        if(state == GameState.Running)
            state = GameState.Paused;
        
        if(board.gameOver) {
            Settings.addScore(board.score);
            Settings.save(game.getFileIO());
        }
    }

    @Override
    public void resume() {
        
    }

    @Override
    public void dispose() {
        
    }
}

